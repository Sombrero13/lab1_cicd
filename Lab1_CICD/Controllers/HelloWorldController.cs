﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab1_CICD.Model;
using Lab1_CICD.Model.TransferEntites;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Lab1_CICD.Controllers
{
    [Route("api/hw")]
    [ApiController]
    public class HelloWorldController : Controller
    {
        private ApplicationDbContext _context;

        public HelloWorldController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/hw
        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            List<User> users = await _context.Users.ToListAsync();

            return users;
        }

        // GET: api/hw/name
        [HttpGet("{name}")]
        public async Task<User> Get(string name)
        {
            User user = await _context.Users.FirstOrDefaultAsync(u => u.Name == name);
                
            return user;
        }

        // POST: api/hw
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TransferUser content)
        {
            try
            {
                _context.Users.Add(new User
                {
                    Name = content.Name,
                    Value = content.Value
                });
                await _context.SaveChangesAsync();

                return Ok($"User {content.Name} was added");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Some error during user addition");
            }
        }
    }
}
