﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lab1_CICD.Model.TransferEntites
{
    public class TransferUser
    {
        public string Name { get; set; }

        public int Value { get; set; }
    }
}
