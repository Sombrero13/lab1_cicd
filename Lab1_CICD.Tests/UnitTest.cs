using Lab1_CICD.Controllers;
using System;
using Xunit;

namespace Lab1_CICD.Tests
{
    public class UnitTest
    {
        VersionController _controller;

        public UnitTest()
        {
            _controller = new VersionController();
        }

        [Fact]
        public void VersionTest()
        {
            string ver = _controller.Get().Result;

            Assert.Equal("0.1", ver);
        }
    }
}
